package com.henry.jvm;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 不同版本jdk常量池在jvm内存中的位置不一样
 * jdk1.6之前,常量池是在永久代中
 * jdk1.7则把常量池移动到了java堆中
 * jdk1.8则去除了永久代的概念,取而代之为元数据区
 *
 * 所以ConstantPoolOOM测试案例   在1.6版本(1.7以下)设置jvm参数  -XX:PermSize=5M -XX:MaxPermSize=7M  会报永久代内存溢出
 *                              在1.7版本设置jvm参数   -Xmx5m  会报Java heap space
 *                              在1.8版本设置jvm参数   -Xmx5m  会报Java heap space
 */
public class ConstantPoolOOM {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        String base = "string";
        int i = 0;
        while (true) {
            i++;
            System.out.println(i);
            //通过intern方法向常量池中手动添加常量
            list.add(String.valueOf(i++).intern());
        }
    }
}
