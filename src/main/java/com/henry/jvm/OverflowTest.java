package com.henry.jvm;

/**
 * 栈内存溢出  无限递归
 */
public class OverflowTest {
	private  int i=0;
//	private  int b=0;
//	private  int c=0;

    public static void main(String[] args) {
        OverflowTest o=new OverflowTest();
        try {
            o.deepTest();
        } catch (Throwable e) {
            System.out.println("over flow deep:"+o.i);
            e.printStackTrace();
        }
    }
    private void deepTest() {
        ++i;
//        ++b;
//        ++c;
        deepTest();
    }
}
