package com.henry.base.gen;

import java.util.List;

import com.henry.base.util.FileUtil;

public class Test2 {
	public static void main(String[] args) {
		List<String> tables = FileUtil.readFile("E://temp.txt");
		for(String line:tables){
			if(line!=null&&line.length()>0&&!line.startsWith("--")){
				line=line.toUpperCase();
				if(line.contains("WHERE")){
					int index=line.indexOf("WHERE");
					String tabString=line.substring(0, index);
					System.out.println(tabString.trim());
				}
			}
		}
	}
}
