package com.henry.base.enum1;

/**
 * 枚举与switch结合使用
 */
enum Color {
    GREEN,RED, BLUE,BLACK
}
public class EnumDemo3 {

    public static void printName(Color color) {
        switch (color) {
            case BLUE:
                System.out.println("蓝色");
                break;
            case RED:
                System.out.println("红色");
                break;
            case GREEN:
                System.out.println("绿色");
                break;
            default:
                System.out.println("不知道什么颜色");
                break;
        }
    }

    public static void main(String[] args) {
        printName(Color.BLUE);
        printName(Color.RED);
        printName(Color.GREEN);
        printName(Color.BLACK);

    }
}
