package com.henry.base.enum1;

import java.util.Arrays;

public enum Day2 {
    MONDAY("星期一"),
    TUESDAY("星期二"),
    WEDNESDAY("星期三"),
    THURSDAY("星期四"),
    FRIDAY("星期五"),
    SATURDAY("星期六"),
    SUNDAY("星期日");

    private String desc;

    private Day2(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "name: "+this.name()+",desc: "+getDesc();
    }

    public static void main(String[] args) {
        for (Day2 day2 : Day2.values()) {
            System.out.println("name: "+day2.name()+",desc: "+day2.getDesc());
        }

        Arrays.stream(Day2.values()).forEach(System.out::println);
    }
}
