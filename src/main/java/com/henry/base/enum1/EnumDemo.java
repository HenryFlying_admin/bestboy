package com.henry.base.enum1;

import java.util.Arrays;

public class EnumDemo {
    public static void main(String[] args) {
        Week week = Week.FRIDAY;
        System.out.println(week.getDeclaringClass().getCanonicalName());
        Week week1 = Enum.valueOf(Week.class, "WEDNESDAY");
        System.out.println(week1.ordinal());
        System.out.println(week1.name());
        Week monday = Week.valueOf("MONDAY");
        monday.ordinal();
        monday.compareTo(week1);
        System.out.println(monday);
        Week[] weeks = monday.values();
        System.out.println(Arrays.toString(weeks));

        Enum e = Week.valueOf("SUNDAY");
//        e.values();
        Class clz = e.getDeclaringClass();
        if (clz.isEnum()) {
            Object[] enumConstants = clz.getEnumConstants();
            System.out.println(Arrays.toString(enumConstants));
        }
    }
}
enum Week {
    MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY
}
