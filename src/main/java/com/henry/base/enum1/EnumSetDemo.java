package com.henry.base.enum1;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class EnumSetDemo {
    public static void main(String[] args) {
        //创建一个空集合
        EnumSet<Color> enumSet = EnumSet.noneOf(Color.class);
        System.out.println("添加元素前："+enumSet.toString());

        enumSet.add(Color.BLACK);
        enumSet.add(Color.BLUE);
        enumSet.add(Color.RED);
        enumSet.add(Color.GREEN);
        System.out.println("添加元素后："+enumSet.toString());

        System.out.println("------------------------");
        //使用allOf创建包含所有枚举实例的enumSet
        EnumSet<Color> enumSet1 = EnumSet.allOf(Color.class);
        System.out.println("allOf直接填充："+enumSet1.toString());
        System.out.println("------------------------");

        //使用range创建指定范围的enumSet   from para1 to para2   按照枚举定义的顺序,顾头顾尾
        EnumSet<Color> enumSet2 = EnumSet.range(Color.BLUE,Color.BLACK);
        System.out.println("指定初始化范围："+enumSet2.toString());
        System.out.println("------------------------");

        //使用complementOf创建指定补集的enumSet，也就是从全部枚举类型去除参数集合中的元素
        EnumSet<Color> enumSet3 = EnumSet.complementOf(enumSet2);
        System.out.println("指定枚举set补集："+enumSet3.toString());
        System.out.println("------------------------");

        //使用of创建指定元素的enumSet
        EnumSet<Color> enumSet4 = EnumSet.of(Color.BLACK,Color.RED);
        System.out.println("指定初始化元素："+enumSet4.toString());
        System.out.println("------------------------");


        //使用copyOf创建复制元素的enumSet
        EnumSet<Color> enumSet5 = EnumSet.copyOf(enumSet4);
        System.out.println("复制enum4："+enumSet5.toString());
        System.out.println("------------------------");

        List<Color> list = new ArrayList<>();
        list.add(Color.BLACK);
        list.add(Color.BLACK);
        list.add(Color.RED);
        list.add(Color.BLUE);
        list.add(Color.BLUE);

        System.out.println(list.toString());

        //使用copyOf创建复制Collection的enumSet
        EnumSet<Color> enumSet6 = EnumSet.copyOf(list);
        System.out.println("复制包含重复元素的list："+enumSet6.toString());
        System.out.println("------------------------");
    }
}
