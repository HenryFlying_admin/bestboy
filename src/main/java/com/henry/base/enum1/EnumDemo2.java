package com.henry.base.enum1;

/**
 * 枚举类定义抽象方法
 * 1.enum关键字前面的abstract不是必须的
 * 2.每个枚举实例都必须实现该抽象方法
 * 3.实现了定义每个枚举实例的不同行为方式
 * 4.枚举实例表现出了多态的特性，但是枚举类型的实例不能作为类型传递使用，编译会报错
 *
 * 5.枚举实现接口（可以实现多接口，与普通class一样），不再举例
 */
public enum EnumDemo2 {

    FIRST{
        @Override
        public String getInfo() {
            return "FIRST TIME";
        }
    },
    SECOND {
        @Override
        public String getInfo() {
            return "SECOND TIME";
        }
    };

    public abstract String getInfo();

    public static void main(String[] args) {
        System.out.println("F:"+EnumDemo2.FIRST.getInfo());
        System.out.println("S:"+EnumDemo2.SECOND.getInfo());
    }
}
