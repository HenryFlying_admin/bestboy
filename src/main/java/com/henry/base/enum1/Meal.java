package com.henry.base.enum1;

import java.util.Arrays;

public enum Meal {
    APPETIZER(Food.Appetizer.class),
    MAINCOURSE(Food.MainCourse.class),
    DESSERT(Food.Dessert.class),
    COFFEE(Food.Coffee.class);
    private Food[] values;


    private Meal(Class<? extends Food> kind) {
        values = kind.getEnumConstants();
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(APPETIZER.values));
    }
}
