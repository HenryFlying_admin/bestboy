package com.henry.base.enum1;

public interface Food {
    //开胃菜
    enum Appetizer implements Food {
        SALAD,SOUP,SPRING_ROLLS;
    }

    enum MainCourse implements Food {
        LASAGNE,BURRITO,PAD_THAI,LENTILS,HUMMOUS,VINDALOO;
    }

    enum Dessert implements Food {
        TIRAMISU,GELATO,BLACK_FOREST_FRUIT,CREME_CARAMEL;
    }

    enum Coffee implements Food {
        BLACK_COFFEE,DECAF_COFFEE,ESPRESSO,LATTE,CAPPUCCINO,TEA,HERB_TEA;
    }
}
