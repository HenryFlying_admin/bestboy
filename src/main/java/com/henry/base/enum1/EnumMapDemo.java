package com.henry.base.enum1;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

/**
 * 1.枚举map的key类型必须为枚举类型
 * 2.key不能为null
 * 3.性能很高，因为枚举的数量是有限的，所以EnumMap底层采用的是数组的方式（两个数组，一个是所有可能的key，另一个是key对应的值）
 */
public class EnumMapDemo {

    public static class Clothes {
        private String id;
        private Color color;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        public Clothes(String id, Color color) {
            this.id = id;
            this.color = color;
        }
    }


    public static void main(String[] args) {
        List<Clothes> list = new ArrayList<>();
        list.add(new Clothes("C001", Color.BLACK));
        list.add(new Clothes("C002", Color.RED));
        list.add(new Clothes("C003", Color.BLACK));
        list.add(new Clothes("C004", Color.RED));
        list.add(new Clothes("C005", Color.BLACK));
        list.add(new Clothes("C006", Color.BLUE));
        list.add(new Clothes("C007", Color.BLUE));
        list.add(new Clothes("C008", Color.BLACK));
        list.add(new Clothes("C009", Color.GREEN));
        list.add(new Clothes("C010", Color.BLACK));
        list.add(new Clothes("C011", Color.RED));
        list.add(new Clothes("C012", Color.BLACK));
        list.add(new Clothes("C013", Color.BLACK));
        list.add(new Clothes("C014", Color.RED));

        EnumMap<Color,Integer> enumMap = new EnumMap<Color,Integer>(Color.class);

        for (Clothes clothes : list) {
            Color color = clothes.getColor();
            Integer count = enumMap.get(color);
            if (count==null)
                enumMap.put(color, 1);
            else
                enumMap.put(color, count + 1);
        }

        System.out.println(enumMap.toString());
    }


}
