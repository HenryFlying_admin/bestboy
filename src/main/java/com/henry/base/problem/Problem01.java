package com.henry.base.problem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Problem01 {
	public static void main(String[] args) {
		String path="E:\\work\\problem\\01\\";
		File file1 = new File(path+"1.txt");
		File file2 = new File(path+"2.txt");
		
		List<String> list1=dealFile(file1);
		List<String> list2=dealFile(file2);
		list1.removeAll(list2);
		System.out.println(list1);
		
		
	}
	
	public static List<String> dealFile(File file){
		try{
			List<String> list = new ArrayList<>();
			FileReader reader=new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(reader);
			String tempString = null;
			while((tempString=bufferedReader.readLine())!=null){
				String s=tempString.split("\t")[0];
				list.add(s);
			}
			bufferedReader.close();
			return list;
		
		}catch(Exception e){
			System.out.println(e.getStackTrace());
		}
		
		return null;
	}
}
