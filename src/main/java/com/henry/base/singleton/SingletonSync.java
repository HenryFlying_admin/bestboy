package com.henry.base.singleton;

/**
 * 懒汉式
 * 1.静态变量instance使用了volatile修饰，避免读线程缓存时，出现不一致的现象
 * 2.volatile+synchornized保证了多线程安全
 * 3.静态方法直接用synchornized修饰，效率较低
 */
public class SingletonSync {
    private static volatile SingletonSync instance;

    private SingletonSync() {
    }

    public static synchronized SingletonSync getInstance() {
        if (instance == null) {
            instance = new SingletonSync();
        }
        return instance;
    }
}
