package com.henry.base.singleton;

/**
 * 枚举单例  代码超级简洁
 * 缺点：占用内存大
 */
public enum SingletonEnum {
    INSTANCE;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
