package com.henry.base.singleton;

/**
 * 双重检查锁机制，保证线程安全的同时，也保证了的高并发的性能
 *
 */
public class SingletonDoubleCheck {
    private static volatile SingletonDoubleCheck instance;

    private SingletonDoubleCheck() {
    }

    public static SingletonDoubleCheck getInstance() {
        if (instance == null) {
            synchronized (SingletonDoubleCheck.class) {
                if (instance==null)
                    instance = new SingletonDoubleCheck();
            }
        }
        return instance;
    }
}
