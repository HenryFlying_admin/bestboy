package com.henry.base.singleton;

/**
 * JDK1.5以前不支持volatile关键字
 * 所以采用jdk类加载机制实现(初始只会加载SingletonInnerClass，只有调用getInstance时才会加载静态内部类，而且静态内部类只会加载一次就实现了懒加载+线程安全+高并发性能)
 */
public class SingletonInnerClass {

    private SingletonInnerClass() {
    }

    private static class Single {
        private static SingletonInnerClass instance = new SingletonInnerClass();
    }

    public static SingletonInnerClass getInstance() {
        return Single.instance;
    }
}

