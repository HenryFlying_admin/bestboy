package com.henry.base.thread;

public class MigrateTask {
	private String name;
	private int taskSize;
	
	public MigrateTask(String name, int taskSize) {
		super();
		this.name = name;
		this.taskSize = taskSize;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTaskSize() {
		return taskSize;
	}
	public void setTaskSize(int taskSize) {
		this.taskSize = taskSize;
	}
	
	
}
