package com.henry.base.thread;

public abstract class Thread1 extends Thread{
	
	
	@Override
	public void run(){
		while(check()){
			deal();
		}
	}
	
	public abstract void deal();
	
	public abstract boolean check();

}
