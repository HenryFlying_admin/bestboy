package com.henry.base.thread;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyThread implements Runnable{
	private static final Logger logger =  LogManager.getLogger(MyThread.class);
	private ThreadGroup group;
	private int num;
	
	public MyThread(ThreadGroup group,int num){
		this.group=group;
		this.num=num;
	}

	@Override
	public void run() {
        logger.info("线程组："+group.getName()+"中的线程："+num+"正在运行...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("线程组："+group.getName()+"中的线程："+num+"运行结束...");
	}
	
	
}
