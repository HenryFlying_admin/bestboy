package com.henry.base.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class TestThread {
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger(TestThread.class);
        logger.info("程序开始运行");
        ExecutorService threadPool = Executors.newFixedThreadPool(200);
        List<MigrateTask> list = new ArrayList<>();
        list.add(new MigrateTask("Table1",101));
        for(int i=0;i<list.size();i++){
            MigrateTask task = list.get(i);
            ThreadGroup group = new ThreadGroup(task.getName());
            for(int j=0;j<task.getTaskSize();j++){
                MyThread myThread = new MyThread(group, j);
                threadPool.execute(myThread);
            }
        }

        threadPool.shutdown();
        while(true){
            if(threadPool.isTerminated()){
                logger.info("全部子线程执行完毕");
                break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        logger.info("程序运行结束");
	}
}
