package com.henry.base.concurrency.future.jdkfuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class FutureMain {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> future = new FutureTask<>(new RealData("name"));
        ExecutorService pool = Executors.newFixedThreadPool(1);
        pool.submit(future);
        System.out.println("请求完毕");
        long l = System.currentTimeMillis();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("数据="+future.get());
        long l1 = System.currentTimeMillis();
        System.out.println(l1-l);
        pool.shutdown();
    }
}
