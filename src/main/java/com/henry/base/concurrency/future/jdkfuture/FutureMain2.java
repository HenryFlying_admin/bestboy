package com.henry.base.concurrency.future.jdkfuture;

import java.util.concurrent.*;

public class FutureMain2 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(1);
        Future<String> future = pool.submit(new RealData("name"));
        System.out.println("请求完毕");
        long l = System.currentTimeMillis();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("数据="+future.get());
        long l1 = System.currentTimeMillis();
        System.out.println(l1-l);
        pool.shutdown();
    }
}
