package com.henry.base.concurrency.future;

import java.sql.SQLOutput;

public class TestFuture {
    public static void main(String[] args){
        Client client = new Client();
        Data data = client.request("name");
        System.out.println("请求完毕");
        long l = System.currentTimeMillis();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("数据="+data.getResult());
        long l1 = System.currentTimeMillis();
        System.out.println(l1-l);
    }
}
