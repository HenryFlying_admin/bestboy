package com.henry.base.concurrency.future;

public interface Data {
    public String getResult();
}
