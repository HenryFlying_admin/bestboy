package com.henry.base.concurrency.future;

public class Client {
    public Data request(final String queryStr) {
        final FutureData future = new FutureData();
//        new Thread(){}
        new Thread(()->{
            RealData realData = new RealData(queryStr);
            future.setRealData(realData);
        }).start();
        return future;
    }
}
