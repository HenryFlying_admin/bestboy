package com.henry.base.concurrency.future;

public class RealData implements Data {
    private String result;

    public RealData(String content) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            sb.append(content);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        result = sb.toString();
//        this.content = content;
    }

    @Override
    public String getResult() {
        return result;
    }
}
