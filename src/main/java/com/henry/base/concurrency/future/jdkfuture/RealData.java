package com.henry.base.concurrency.future.jdkfuture;

import java.util.concurrent.Callable;

public class RealData implements Callable<String> {
    private String content;

    public RealData(String content) {
        this.content = content;
    }

    @Override
    public String call() throws Exception {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            sb.append(content);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
