package com.henry.base.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class Test1 {
    public static void main(String[] args) throws InterruptedException {
        List<Person> list = new ArrayList<>();
        CyclicBarrier barrier = new CyclicBarrier(5);
        CountDownLatch latch = new CountDownLatch(10);
        TrafficLight light = new TrafficLight(barrier, latch);
        for (int i = 0; i < 10; i++) {
            list.add(new Person(light));
        }
        Driver driver = new Driver(light);
        new Thread(driver).start();
        for (Person p : list) {
            p.setName("p"+(list.indexOf(p)+1));
            Thread t = new Thread(p);
            t.start();
            Thread.sleep(500);//每过500ms来一个人
        }
    }
}
