package com.henry.base.concurrency.aio2;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AioServer {
    private static final Object waitObject = new Object();
    public static void main(String[] args) throws Exception{
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        AsynchronousChannelGroup asynchronousChannelGroup = AsynchronousChannelGroup.withThreadPool(threadPool);
        final AsynchronousServerSocketChannel serverSocketChannel = AsynchronousServerSocketChannel.open(asynchronousChannelGroup);
//        serverSocketChannel.
        serverSocketChannel.bind(new InetSocketAddress(9008));
        serverSocketChannel.accept(null,new ServerSocketChannelHandler(serverSocketChannel));
        synchronized (waitObject) {
            waitObject.wait();
        }
    }


}
