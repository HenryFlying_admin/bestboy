package com.henry.base.concurrency.aio2;

import javax.print.attribute.standard.OrientationRequested;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.TimeUnit;

public class ServerSocketChannelHandler implements CompletionHandler<AsynchronousSocketChannel,Void> {
    private AsynchronousServerSocketChannel serverSocketChannel;

    public ServerSocketChannelHandler(AsynchronousServerSocketChannel serverSocketChannel) {
        this.serverSocketChannel = serverSocketChannel;
    }

    @Override
    public void completed(AsynchronousSocketChannel socketChannel, Void attachment) {
        this.serverSocketChannel.accept(attachment,this);
        ByteBuffer buffer = ByteBuffer.allocate(16);
        socketChannel.read(buffer,3l,TimeUnit.SECONDS, new StringBuffer(), new SocketChannelReadHandler(socketChannel, buffer));

    }

    @Override
    public void failed(Throwable exc, Void attachment) {
        System.out.println("failed");
    }
}
