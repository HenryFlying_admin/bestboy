package com.henry.base.concurrency;

public class PriorityDemo {
    static Integer times1;
    static Integer times2;

    public static class HighPriority extends Thread {
        static int count=0;
        @Override
        public void run() {
            while (true) {
                synchronized (PriorityDemo.class) {
                    count++;
                    if (count > 100000) {
                        System.out.println("HighPriority is completed!");
                        times1=LowPriority.count;
                        break;
                    }
                }
            }
        }
    }

    public static class LowPriority extends Thread {
        static int count=0;

        @Override
        public void run() {
            while (true) {
                synchronized (PriorityDemo.class) {
                    count++;
                    if (count > 100000) {
                        System.out.println("LowPriority is completed!");
                        times2=HighPriority.count;
                        break;
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        HighPriority highPriority = new HighPriority();
        LowPriority lowPriority = new LowPriority();
        highPriority.setPriority(Thread.MAX_PRIORITY);
        lowPriority.setPriority(Thread.MIN_PRIORITY);
        lowPriority.start();
        highPriority.start();
        lowPriority.join();
        highPriority.join();

        System.out.println("highPriority执行完毕时,lowPriority执行次数:"+times1);
        System.out.println("lowPriority执行完毕时,highPriority执行次数:"+times2);
    }
}
