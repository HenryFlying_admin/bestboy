package com.henry.base.concurrency;

public class VolatileDemo {

    private volatile int count = 0;
    volatile int noThreads;


    public int getCount() {
        return this.count;
    }


    public void setCount() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.count++;
    }


    public static void main(String[] args) {
        // TODO Auto-generated method stub
        VolatileDemo demo = new VolatileDemo();
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {demo.setCount();}).start();
        }
        ThreadGroup group = Thread.currentThread().getThreadGroup();

        while ((demo.noThreads = group.activeCount()) > 2) {
            System.out.println("当前线程数:" + demo.noThreads);
            if (demo.noThreads == 2) {
                Thread[] lstThreads = new Thread[demo.noThreads];
                group.enumerate(lstThreads);
                for (int i = 0; i < demo.noThreads; i++)
                    System.out.println("线程号：" + i + " = " + lstThreads[i].getName());
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        System.out.println(demo.getCount());
    }

}
