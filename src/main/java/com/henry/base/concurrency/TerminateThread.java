package com.henry.base.concurrency;

public class TerminateThread implements Runnable{
    @Override
    public void run() {
        while (true) {
            //优雅地终止线程 elegance(判断线程有没有被打上终止标记,如果发现被打上了终止标记,退出循环--此种情况保证了每一次循环的原子性)
            if (Thread.currentThread().isInterrupted()) {
                System.out.println("Interruptd!");
                break;
            }
            //Thread.yield();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                System.out.println("Interrupted When Sleep!");
                //设置中断状态,因为抛出异常会清除线程的中断状态
                Thread.currentThread().interrupt();
            }
            Thread.yield();
        }
    }
}
