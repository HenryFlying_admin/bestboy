package com.henry.base.concurrency;

public class Driver implements Runnable {
    private TrafficLight light;

    public Driver(TrafficLight light) {
        this.light = light;
    }

    @Override
    public void run() {
        System.out.println("司机等红灯...");
        light.driverWaitRedLight();
        System.out.println("司机过马路...嘟嘟嘟...");
    }
}
