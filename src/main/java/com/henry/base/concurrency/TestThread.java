package com.henry.base.concurrency;

import org.junit.Test;

public class TestThread {

    public static class ThreadDemo extends Thread {
        private String name;
        private Thread befThread;

        public ThreadDemo(String name) {
            this.name=name;
        }

        public void setBefThread(Thread befThread) {
            this.befThread = befThread;
        }

        @Override
        public void run() {
            if(this.befThread!=null){
                try {
                    this.befThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(this.name+" is running!");
            System.out.println(this.name+" finished!");
        }
    }


    @Test
    public void testJoin() throws InterruptedException {
        ThreadDemo t1 = new ThreadDemo("t1");
        ThreadDemo t2 = new ThreadDemo("t2");
        t2.setBefThread(t1);
        ThreadDemo t3 = new ThreadDemo("t3");
        t3.setBefThread(t2);
        t1.start();
        t3.start();
        t2.start();
//        t1.start();
//        t1.join();
//        t2.start();
//        t2.join();
//        t3.start();
//        t3.join();
    }

}
