package com.henry.base.concurrency.lockfree;

import org.junit.Test;

public class TestLockFreeStack {
    static LockFreeStack<Object> stack = new LockFreeStack<>();

    static class Push extends Thread {
        public void run() {
            for (; ; ) {
                stack.push("Random->" + Math.random());
            }
        }
    }

    static class Pop extends Thread {
        @Override
        public void run() {
            for (; ; ) {
                System.out.println("已出栈:" + stack.pop());
            }
        }
    }

    @Test
    public void test01() throws Exception {
        Push[] pushs = new Push[10];
        Pop[] pops = new Pop[10];
        for (int i = 0; i < 10; i++) {
            pushs[i]=new Push();
            pops[i] = new Pop();
        }

        for (int i = 0; i < 10; i++) {
            pushs[i].start();
            pops[i].start();
        }

        for (int i = 0; i < 10; i++) {
            pushs[i].join();
            pops[i].join();
        }
    }
}
