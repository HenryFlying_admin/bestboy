package com.henry.base.concurrency.nio;

import org.junit.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TestIterator {

    @Test
    public void fun1() {
        Set<Integer> set = new HashSet<>();
        set.add(3);
        set.add(1);
        set.add(9);

        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            iterator.remove();
        }
    }
}
