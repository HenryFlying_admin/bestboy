package com.henry.base.concurrency;

import java.util.concurrent.*;

public class TestQueuePerformance {
    private static final int capacity = 2000000;
    private static final int singleCapacity = 100000;
    private static final int threadNum = 8;
    public static CountDownLatch latch = new CountDownLatch(threadNum);
    static long begin;
    static long end;

    public static class TestArrayBlockingQueue implements Runnable {
        static ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(capacity);

        @Override
        public void run() {
            try {
                for (int i = 0; i < singleCapacity; i++) {
//                    Thread.sleep(1);
//                    String s = "dasfafafs";
//                    int sum=0;
//                    for (char c : s.toCharArray()) {
//                        byte x = (byte) c;
//                        sum+=x;
//                    }
                    queue.offer(i);
                }
//                System.out.println("...............");
            } finally {
                latch.countDown();
            }
        }
    }


    public static class TestLinkedBlockingQueue implements Runnable {
        static LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>(capacity);

        @Override
        public void run() {
            try {
                for (int i = 0; i < singleCapacity; i++) {
//                    Thread.sleep(1);
//                    String s = "dasfafafs";
//                    int sum=0;
//                    for (char c : s.toCharArray()) {
//                        byte x = (byte) c;
//                        sum+=x;
//                    }
                    queue.offer(i);
                }
            } finally {
                latch.countDown();
            }
        }
    }

    public static class TestConcurrentLinkedQueue implements Runnable {
        static ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<Integer>();

        @Override
        public void run() {
            try {
                for (int i = 0; i < singleCapacity; i++) {
//                    Thread.sleep(1);
//                    String s = "dasfafafs";
//                    int sum=0;
//                    for (char c : s.toCharArray()) {
//                        byte x = (byte) c;
//                        sum+=x;
//                    }
                    queue.offer(i);
                }

            } finally {
                latch.countDown();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(threadNum);
        begin= System.nanoTime();
        for (int i = 0; i < threadNum; i++) {
            pool.submit(new TestArrayBlockingQueue());
        }
        latch.await();
        end= System.nanoTime();
        System.out.println("ArrayBlockingQueue花费时间："+(end-begin)/1000/1000+"ms");

        latch=new CountDownLatch(threadNum);
        begin= System.nanoTime();
        for (int i = 0; i < threadNum; i++) {
            pool.submit(new TestLinkedBlockingQueue());
        }
        latch.await();
        end= System.nanoTime();
        System.out.println("LinkedBlockingQueue花费时间："+(end-begin)/1000/1000+"ms");

        latch=new CountDownLatch(threadNum);
        begin= System.nanoTime();
        for (int i = 0; i < threadNum; i++) {
            pool.submit(new TestConcurrentLinkedQueue());
        }
        latch.await();
        end= System.nanoTime();
        System.out.println("ConcurrentLinkedQueue花费时间："+(end-begin)/1000/1000+"ms");

        pool.shutdown();
    }

}
