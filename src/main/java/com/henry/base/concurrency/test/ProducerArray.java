package com.henry.base.concurrency.test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

public class ProducerArray implements Runnable {

    //容器
    private final ArrayBlockingQueue<Bread> queue;
    private final CountDownLatch cdl;
    public ProducerArray(ArrayBlockingQueue<Bread> queue, CountDownLatch cdl) {
        this.queue = queue;
        this.cdl = cdl;
    }

    @Override
    public void run() {
        for(int i=0;i<100000; i++){
            produce(i);
        }
        cdl.countDown();
    }

    public void produce(int i){
        /**
         * put()方法是如果容器满了的话就会把当前线程挂起
         * offer()方法是容器如果满的话就会返回false。
         */
        try {
            Bread bread = new Bread();
            bread.setName(""+i);
            queue.offer(bread);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
