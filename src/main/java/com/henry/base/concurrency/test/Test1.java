package com.henry.base.concurrency.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.*;

public class Test1 {
    static final int threadNum = 30;
    static final int dealNum = 3000000;
//    static CyclicBarrier cyclic = new CyclicBarrier(threadNum);
//    static List<Integer> list = new CopyOnWriteArrayList<>();
//    static List<Integer> list = new ArrayList<>();
//    static List<Integer> list = Collections.synchronizedList(new ArrayList<>());


    public static class UnSafeArrayList implements Runnable {
        public static volatile List<Integer> list = new ArrayList<>();
        public static CountDownLatch latch = new CountDownLatch(threadNum);
        final int i;

        public UnSafeArrayList(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            try {
                for (int j = 0; j < dealNum; j++) {
                    if (j % threadNum == this.i)
                        list.add(this.i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
        }
    }

    public static class SyncArrayList implements Runnable {
        public static List<Integer> list = Collections.synchronizedList(new ArrayList<>());
        public static CountDownLatch latch = new CountDownLatch(threadNum);
        final int i;

        public SyncArrayList(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            try {
                for (int j = 0; j < dealNum; j++) {
                    if (j % threadNum == this.i)
                        list.add(this.i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
        }
    }

    public static class SafeCopyOnWriteArrayList implements Runnable {
        public static List<Integer> list = new CopyOnWriteArrayList<>();
        public static CountDownLatch latch = new CountDownLatch(threadNum);
        final int i;

        public SafeCopyOnWriteArrayList(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            try {
                for (int j = 0; j < dealNum; j++) {
                    if (j % threadNum == this.i)
                        list.add(this.i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
        }
    }

    public static class SafeVector implements Runnable {
        public static List<Integer> list = new Vector<>();
        public static CountDownLatch latch = new CountDownLatch(threadNum);
        final int i;

        public SafeVector(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            try {
                for (int j = 0; j < dealNum; j++) {
                    if (j % threadNum == this.i)
                        list.add(this.i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
        }
    }


    public static void main(String[] args) throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(threadNum);
        test1(threadPool,UnSafeArrayList.class);
        test1(threadPool,SyncArrayList.class);
//        test1(threadPool,SafeCopyOnWriteArrayList.class);
        test1(threadPool,SafeVector.class);
        threadPool.shutdown();
    }

    public static void test1(ExecutorService threadPool, Type type) {

        long begin = System.nanoTime();
        try {
//            String typeName = type.getTypeName();
//            Class<?> clz = Class.forName(typeName);
            Class clz = (Class) type;
//            Field[] fields = clz.getDeclaredFields();
//            System.out.println(Arrays.toString(fields));

            CountDownLatch latch = (CountDownLatch) clz.getDeclaredField("latch").get(type);
            List list = (List) clz.getDeclaredField("list").get(type);
            Constructor constructor=clz.getConstructor(int.class);
            for (int i = 0; i < threadNum; i++) {
                Runnable instance = (Runnable) constructor.newInstance(i);
                threadPool.submit(instance);
            }
            latch.await();
            long end = System.nanoTime();
            System.out.println(clz.getTypeName()+": "+(end - begin) / 1000 / 1000 + "ms    ---->"+list.size());
//            System.out.println();
//        end.await();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
    }
}
