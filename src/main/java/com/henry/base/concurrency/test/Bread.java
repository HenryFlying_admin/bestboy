package com.henry.base.concurrency.test;

public class Bread {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
