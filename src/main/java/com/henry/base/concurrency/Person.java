package com.henry.base.concurrency;

public class Person implements Runnable {
    private TrafficLight light;
    private String name;

    public Person(TrafficLight light) {
        this.light = light;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(name+"等红灯...");
        light.peopleWaitRedLight();
        System.out.println(name+"过马路...哒哒哒哒....");
    }
}
