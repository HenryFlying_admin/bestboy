package com.henry.base.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {
	public static List<String> readFile(String path) {
		List<String> list = new ArrayList<>();
		try {
			File file = new File(path);
//			FileReader reader = new FileReader(file);
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=reader.readLine())!=null){
				list.add(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list; 
	}
	
}
