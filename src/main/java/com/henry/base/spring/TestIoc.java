package com.henry.base.spring;

public class TestIoc {
    public static void main(String[] args) throws Exception{
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Object user1 = context.getBean("user1");
        Object user2 = context.getBean("user2");
    }
}
