package com.henry.base.spring;

public interface BeanFactory {
    public Object getBean(String name);
}
