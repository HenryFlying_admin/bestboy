package com.henry.base.java8.defaultimpl;

public class Car implements Vehicle,FourWheeler {
    @Override
    public void print() {
        System.out.println("我只是一辆车！");
    }

    @Override
    public void print2() {
        FourWheeler.super.print2();
    }
}
