package com.henry.base.java8.defaultimpl;

public interface Vehicle {
    default void print(){
        System.out.println("我是一辆卡车！");
    }

    static void blowHorn() {
        System.out.println("按喇叭！！！");
    }

    default void print1(String x) {
        System.out.println(x+"...");
    }

    void print2();
}
