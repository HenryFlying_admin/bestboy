package com.henry.base.java8.defaultimpl;

public class Car1 implements Vehicle,FourWheeler {
    @Override
    public void print() {
        Vehicle.super.print();
        FourWheeler.super.print();
        Vehicle.blowHorn();
        System.out.println("我是一辆汽车！");
    }

    @Override
    public void print2() {
        FourWheeler.super.print2();
    }
}
