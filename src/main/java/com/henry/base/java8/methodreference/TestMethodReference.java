package com.henry.base.java8.methodreference;

import java.util.ArrayList;
import java.util.List;

public class TestMethodReference {
    public static void main(String[] args) {
        List names = new ArrayList();
        names.add("Google");
        names.add("Baidu");
        names.add("Tencent");
        names.add("Alibaba");
        names.add("NetEase");

        names.forEach(System.out::println);
    }
}
