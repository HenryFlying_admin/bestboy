package com.henry.base.java8.methodreference;


import java.util.Arrays;
import java.util.List;

public class TestCar {
    public static void main(String[] args) {
//        new TestCar().testConstructorReference();
//        new TestCar().testStaticMethodReference();
//        new TestCar().testNonStaticMethodReference();
        new TestCar().testInstanceMethodReference();
    }


    public void testConstructorReference() {
        //构造器引用，语法Class::new,或者更一般的Class<T>::new
        //3种方式,第一种：方法引用+Lambda表达式
//        final Car car = Car.create(Car::new);
        //第二种：Lambda表达式
        final Car car = Car.create(()->new Car());
        //第三种：常规的匿名内部类
//        final Car car = Car.create(new Supplier<Car>() {
//            @Override
//            public Car get() {
//                return new Car();
//            }
//        });
        final List<Car> cars = Arrays.asList(car);
    }

    public void testStaticMethodReference() {
        final Car car = Car.create(Car::new);
        final Car car2 = Car.create(Car::new);
        List<Car> cars = Arrays.asList(car, car2);
        //静态方法引用
        cars.forEach(Car::collide);
    }

    public void testNonStaticMethodReference(){
        final Car car = Car.create(Car::new);
        System.out.println(car);
        List<Car> cars = Arrays.asList(car);
        //实例方法引用，--〉特定类的任意对象的方法引用
        // 每个对象调用自己的repair
        cars.forEach(Car::repair);
    }

    //特定类的特定对象的方法引用
    public void testInstanceMethodReference(){
        final Car car = Car.create(Car::new);
        final Car car2 = Car.create(Car::new);
        System.out.println(car);
        List<Car> cars = Arrays.asList(car,car2);
        final Car police=Car.create(Car::new);
        System.out.println("police... "+police);
        //police对象调用follow方法，该方法还有一个参数，默认把当前对象当作方法参数传进去
        cars.forEach(police::follow);
    }


}
