package com.henry.base.java8.lambda;

public interface MathOperation {
    int operation(int a, int b);
}
