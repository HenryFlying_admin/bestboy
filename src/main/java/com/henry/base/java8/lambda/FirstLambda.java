package com.henry.base.java8.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FirstLambda {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Jame");
        names.add("Yun");
        names.add("Henry");
        names.add("Tom");
        names.add("Sara");
        FirstLambda firstLambda = new FirstLambda();
        firstLambda.sortUsingLambda(names);
        System.out.println(names);
    }

    private void sortUsingLambda(List<String> names) {
        Collections.sort(names,(s1,s2)->s1.compareTo(s2));
    }
}
