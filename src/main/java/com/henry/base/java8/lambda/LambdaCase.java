package com.henry.base.java8.lambda;

import com.henry.base.java8.lambda.pojo.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdaCase {
    static List<User> list = new ArrayList<>();
    static {
        User user1 = new User("Jame",18);
        User user2 = new User("Yun",22);
        User user3 = new User("Henry",18);
        User user4 = new User("Tom",30);
        User user5 = new User("Sara",21);
        User user6 = new User("Tale",28);

        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);
        list.add(user6);
    }
    public static void main(String[] args) {
        LambdaCase lambdaCase = new LambdaCase();
        Collections.shuffle(list);
        System.out.println("洗牌后："+list);
        lambdaCase.sortUserByAnonymousClass(list);
        System.out.println("排序后："+list+"\r\n***************");

        Collections.shuffle(list);
        System.out.println("洗牌后："+list);
        lambdaCase.sortUserByLambda(list);
        System.out.println("排序后："+list+"\r\n***************");

        Collections.shuffle(list);
        System.out.println("洗牌后："+list);
        lambdaCase.sortUserByLambda1(list);
        System.out.println("排序后："+list+"\r\n***************");
        Collections.shuffle(list);
        System.out.println("洗牌后："+list);
        lambdaCase.sortUserByLambda2(list);
        System.out.println("排序后："+list+"\r\n***************");

    }

    /**
     * 原始方式，匿名内部类
     * @param list
     */
    private void sortUserByAnonymousClass(List<User> list) {
        Collections.sort(list, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                if(o1.getAge()-o2.getAge()!=0)
                    return o1.getAge()-o2.getAge();
                return o1.getName().compareTo(o2.getName());

            }
        });
    }

    /**
     * Lambda表达式   圆括号+参数类型声明->大括号+return+语句+分号结束
     * 最完整的方式
     * @param list
     */
    private void sortUserByLambda(List<User> list){
        Collections.sort(list,(User u1,User u2)->{
            if(u1.getAge()-u2.getAge()!=0)
                return u1.getAge()-u2.getAge();
            return u1.getName().compareTo(u2.getName());
        });
    }

    /**
     * lambda表达式支持参数类型声明推断，所以可以省略参数类型声明
     * @param list
     */
    private void sortUserByLambda1(List<User> list){
        Collections.sort(list,(u1, u2)->{
            if(u1.getAge()-u2.getAge()!=0)
                return u1.getAge()-u2.getAge();
            return u1.getName().compareTo(u2.getName());
        });
    }
    /**
     * lambda表达式支持,如果主体只包含一个语句，则可以省略{}
     * 需注意：省略{}时,return关键字也必须省略，语句结尾的;也必须省略
     * 包含{}时，就是常规的java语法，return根据需要可有可无，结尾的;不能省略
     * @param list
     */
    private void sortUserByLambda2(List<User> list){
        Collections.sort(list,(u1, u2)->u1.getAge()-u2.getAge()!=0?u1.getAge()-u2.getAge():u1.getName().compareTo(u2.getName()));
    }


    /**
     * Lambda表达式   圆括号+参数类型声明->大括号+return+语句+分号结束
     * @param list
     */
    private void sortUserByAge(List<User> list){
        Collections.sort(list,(User u1,User u2)->{
            return u1.getAge()-u2.getAge();
        });
    }

    private void sortUserByAge1(List<User> list){
        Collections.sort(list,( u1, u2)->{
            return u1.getAge()-u2.getAge();
        });
    }

    private void sortUserByAge2(List<User> list){
        Collections.sort(list,( u1, u2)-> u1.getAge()-u2.getAge());
    }

}
