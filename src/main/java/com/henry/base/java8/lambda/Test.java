package com.henry.base.java8.lambda;

import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
//        ReflectUtil.
        String s = "aaabbb";
        String b = new String("aaabbb");
        int hashCode = s.hashCode();
        int hashcode1 = b.hashCode();

        System.out.println(hashCode);
        System.out.println(hashcode1);

        Map<String,Object> map = new HashMap();
        map.put(s, 123);
        map.put(b, "456");

        System.out.println(map);
    }
}
