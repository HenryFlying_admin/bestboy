package com.henry.base.java8.function;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


//        1.函数式接口是指有且仅有一个抽象方法，但是可以有多个非抽象方法的接口
//        1.1也不一定只有一个抽象方法，比如jdk1.8之前的函数式接口Comparator<T>,
//        有抽象方法 int compareTo(T o1,T o2);
//        还有抽象方法 boolean equals(Object obj);  -->该方法为object的方法，有默认实现，所有实现Comparator<T>接口时，可以无视equals方法
//        2.一般会有注解@FunctionalInterface
//
//        Comparator与Comparable接口异同
//        都是用来排序的
//        要排序的类实现Comparator接口，可以直接list.sort();   缺点：排序方式只有一种，如果要排序的类没有实现Comparator接口，无法排序，修改的话，就对源代码造成了入侵
//        Comparator接口
//        很灵活，只需要在调用排序方法的时候，实现Comparator接口即可

//测试函数式接口，以Predicate为例
public class TestPredicate {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        System.out.println("输出所有数据：");
        eval(list, n -> true);
        System.out.println("输出所有偶数：");
        eval(list,n->n%2==0);
        System.out.println("输出所有大于3的数：");
        eval(list,n->n>3);
//        sadasdad
    }


    public static void eval(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer n : list) {
            if (predicate.test(n)) {
                System.out.println(n+"");
            }
        }
    }
}
