package com.henry.base.java8.date;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TestDate {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("当前时间："+now);
        LocalDate localDate = now.toLocalDate();
        System.out.println("当前日期："+localDate);
        Month month = now.getMonth();
        int day = now.getDayOfMonth();
        DayOfWeek week = now.getDayOfWeek();
        int second = now.getSecond();
        System.out.println("月："+month+",日："+day+",星期："+week+",秒："+second);


        LocalDateTime date2 = now.withHour(15).withMinute(0).withSecond(0).withNano(0).withDayOfMonth(1).withYear(2012);
        System.out.println(date2);

        //
        LocalDate localDate1 = LocalDate.of(2014, Month.DECEMBER, 12);
        System.out.println(localDate1);
        LocalTime localTime = LocalTime.of(22, 15, 00, 0000000);
        System.out.println(localTime);

        //解析字符串
        LocalTime localTime1 = LocalTime.parse("20:15:30");
        System.out.println(localTime1);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss SSS");
        LocalDateTime localDateTime = LocalDateTime.parse("2018-10-30 12:31:28 980", dateTimeFormatter);
        System.out.println(localDateTime);
        System.out.println(localDateTime.format(dateTimeFormatter));

        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());

//        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2015-12-03T10:15:30+05:30[Asia/Shanghai]");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2015-12-03T10:15:30+08:00");
        System.out.println("zonedDateTime: "+zonedDateTime);
        System.out.println(zonedDateTime.getZone());

        ZoneId zoneId = ZoneId.of("Europe/Paris");
        System.out.println("zoneId: "+zoneId);

        ZoneId currentZone = ZoneId.systemDefault();
        System.out.println("当前时区： "+currentZone);

    }
}
