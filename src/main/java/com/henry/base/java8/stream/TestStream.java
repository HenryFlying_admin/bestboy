package com.henry.base.java8.stream;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class TestStream {
    public static void main(String[] args) {
//        new TestStream().run2();
//        new TestStream().run3();
//        new TestStream().run4();
//        new TestStream().run5();
//        new TestStream().run6();
//        new TestStream().run7();
//        new TestStream().run8();
        new TestStream().run9();

    }
    //生成流stream
    public void run1(){
        List<String> strings = Arrays.asList("sdaad", "sf", "sss", "qweq");
        strings.stream();
        strings.parallelStream();

    }

    //forEach
    public void run2(){
        List<String> strings = Arrays.asList("sdaad", "sf", "sss", "qweq");
        strings.stream().forEach(System.out::println);
        System.out.println("*****************");
        strings.forEach(System.out::println);
    }

    //map
    public void run3(){
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        //获取对应的平方数
        List<Integer> squaresList = numbers.stream().map(integer -> integer * integer).distinct().collect(Collectors.toList());
        squaresList.forEach(System.out::println);
    }
    //filter
    public void run4(){
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
        long count = strings.stream().filter(string -> string.isEmpty()).count();
        System.out.println(count);
    }
    //limit 获取指定数量的流
    public void run5(){
        Random random = new Random();
        random.ints(1,20).limit(10).forEach(System.out::println);
    }

    //sorted
    public void run6() {
        Random random = new Random();
        random.ints(1,20).limit(10).sorted().forEach(System.out::println);
    }

    //parallel 流并行处理程序的代替方法
    public void run7(){
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
        long count = strings.parallelStream().filter(string -> string.isEmpty()).count();
        System.out.println(count);
    }
    //Collectors 实现了很多规约操作，比如将流转换成集合和聚合元素。Collectors可用于返回列表或字符串
    public void run8(){
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
        List<String> filteredList = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println("筛选列表： "+filteredList);
        String mergedString = strings.stream().filter(s -> !s.isEmpty()).collect(Collectors.joining(", "));
        System.out.println("合并后字符串： "+mergedString);
    }

    //statistics 统计
    public void run9(){
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        IntSummaryStatistics statistics = numbers.stream().mapToInt((x) -> x).summaryStatistics();
        System.out.println("列表中最大的数 ： "+statistics.getMax());
        System.out.println("列表中最小的数 ： "+statistics.getMin());
        System.out.println("列表中所有数之和 ： "+statistics.getSum());
        System.out.println("列表中平均数 ： "+statistics.getAverage());
    }


}
