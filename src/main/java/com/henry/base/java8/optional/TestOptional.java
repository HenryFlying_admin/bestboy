package com.henry.base.java8.optional;

import java.util.Optional;

public class TestOptional {
    public static void main(String[] args) {
        Integer v1 = null;
        Integer v2 = new Integer(10);
        //ofNullable方法允许传递的参数是null
        Optional<Integer> a = Optional.ofNullable(v1);
        //of方法，如果传递的参数是null，会抛出异常NullPointerException
        Optional<Integer> b = Optional.of(v2);
        System.out.println(new TestOptional().sum(a,b));
    }


    public Integer sum(Optional<Integer> a,Optional<Integer> b) {
        //optional.isPresent -->判断值是否存在
        System.out.println("第一个参数值存在："+a.isPresent());
        System.out.println("第二个参数值存在："+b.isPresent());
        //optional.orElse  -->如果值存在，返回它，否则返回默认值
        Integer value1 = a.orElse(new Integer(0));
        //optional.get  -->获取值，值需要存在
        Integer value2 = b.get();
        return value1+value2;
    }
}
