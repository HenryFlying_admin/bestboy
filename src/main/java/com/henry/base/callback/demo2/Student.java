package com.henry.base.callback.demo2;

public class Student {
	private String name;
	public Student(String name) {
		super();
		this.name = name;
	}
	public Student() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void callHelp(int a,int b){
		new SuperCalculator().add(a, b, new DoHomework());
	}
	public class DoHomework implements DoJob{

		@Override
		public void fillBlank(int a, int b, int result) {

			System.out.println(name+"����С����㣺"+a+" + "+b+" = "+result);
			
		}
	}

	
}
