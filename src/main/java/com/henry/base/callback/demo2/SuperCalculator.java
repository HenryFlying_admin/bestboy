package com.henry.base.callback.demo2;

public class SuperCalculator {
	public void add(int a,int b,DoJob customer)  {
		int result = a+b;
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		customer.fillBlank(a, b, result);
	}
}
