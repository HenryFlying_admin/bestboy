package com.henry.base.callback.demo2;


public class Seller {
	private String name;
	public Seller(String name) {
		super();
		this.name = name;
	}
	public Seller() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void callHelp(int a,int b){
		new SuperCalculator().add(a, b, new DoSellWork());
	}
	public class DoSellWork implements DoJob{
		@Override
		public void fillBlank(int a, int b, int result) {
			// TODO Auto-generated method stub
			System.out.println(name+"����С�����ˣ�"+a+" + "+b+" = "+result);
			
		}
	}
}
