package com.henry.extension.logging;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
/**
 * 
 * asyn:
 * test1=747 test2=777 test3=756  test4=737 3818  test5=745 3766  test6=4174
 * sync:
 * test1=849 test2=834 test3=871  test4=741 6154  test5=679 5978  test6=6523
 * 
 * asyn:1000000
 * test1=747 test2=777 test3=756  test4=30594  test5=30607  test6=33763
 * sync:
 * test1=849 test2=834 test3=871  test4=52195  test5=52049  test6=55342
 * test1:705 731
 * test2:697 725
 *
 */

public class LogTest {
	StringBuffer msg;//20000W字符
	StringBuffer msg1;
	@Before
	public void init(){
//		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		msg = new StringBuffer();
		for(int i = 1;i < 1000000;i++){
			msg.append("abcdefghijpoiuytrew");
		}
		msg1 = new StringBuffer();
		for(int i = 1;i < 10;i++){
			msg1.append("abcdefghijpoiuytrew");
		}
		
	}

	@Test
	public void test1() {
		long s = System.currentTimeMillis();
		Logger logger = LogManager.getLogger("services");
		System.out.println("getlogger:"+(System.currentTimeMillis()-s));
		System.out.println("asdafafaafaf");
		long e = System.currentTimeMillis();
		System.out.println("test1:time="+(e-s));
	}
	
	@Test
	public void test2(){
		long s = System.currentTimeMillis();
		Logger logger = DynamicAddLogger.getLogger("log/test2");
		System.out.println("getlogger:"+(System.currentTimeMillis()-s));
		System.out.println(msg);
		long e = System.currentTimeMillis();
		System.out.println("test2:time="+(e-s));
	}
	
	@Test
	public void test3(){
		long s = System.currentTimeMillis();
		DynamicAddLogger.testLogPrint(msg.toString());
		long e = System.currentTimeMillis();
		System.out.println("test3:time="+(e-s));
	}
	
	@Test
	public void test4(){
		long s = System.currentTimeMillis();
		Logger logger = LogManager.getLogger("services");
		System.out.println("getlogger:"+(System.currentTimeMillis()-s));
		for(int i = 1;i <= 1000000;i++){
			logger.trace(msg1);
		}
		long e = System.currentTimeMillis();
		System.out.println("test1:time="+(e-s));
		
	}
	
	@Test
	public void test5(){
		long s = System.currentTimeMillis();
		Logger logger = DynamicAddLogger.getLogger("log/test5");
		System.out.println("getlogger:"+(System.currentTimeMillis()-s));
		for(int i = 1;i <= 1000000;i++){
			logger.trace(msg1);
		}
		long e = System.currentTimeMillis();
		System.out.println("test1:time="+(e-s));
		
	}
	
	@Test
	public void test6(){
		long s = System.currentTimeMillis();
		System.out.println("getlogger:"+(System.currentTimeMillis()-s));
		for(int i = 1;i <= 1000000;i++){
			DynamicAddLogger.testLogPrint(msg1.toString());
		}
		long e = System.currentTimeMillis();
		System.out.println("test1:time="+(e-s));
		
	}

}
