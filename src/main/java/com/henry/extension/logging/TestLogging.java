package com.henry.extension.logging;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.spi.ExtendedLogger;
import org.apache.logging.log4j.spi.ExtendedLoggerWrapper;

import java.util.Calendar;

public class TestLogging {
    private static final Logger logger = LogManager.getLogger(TestLogging.class);
    public static void main(String[] args) {
        ExtendedLoggerWrapper loggerWrapper = new ExtendedLoggerWrapper((ExtendedLogger) logger,"abc",null);
        loggerWrapper.logIfEnabled(TestLogging.class.getName(),Level.DEBUG,null,"dadadad");
        long begin = Calendar.getInstance().getTimeInMillis();
        for (int i = 0; i < 10; i++) {
            logger.trace("trace message " + i);
            System.out.println("debug message " + i);
            System.out.println("info message " + i);
            logger.warn("warn message " + i);
            logger.error("error message " + i);
            logger.fatal("fatal message " + i);
        }
        long end = Calendar.getInstance().getTimeInMillis();
        System.out.println("Hello World! 2");
    }
}
