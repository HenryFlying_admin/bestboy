package com.henry.extension.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AbstractOutputStreamAppender;
import org.apache.logging.log4j.core.appender.OutputStreamManager;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class CbppLogManager {
    private static final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
    private static final Configuration config = ctx.getConfiguration();
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Logger logger = getLogger("test.log");
        long end = System.currentTimeMillis();
        System.out.println(end - start);

    }

    public static Logger getLogger(String filename) {
        LoggerConfig loggerConfig = config.getLoggerConfig("services");
        AbstractOutputStreamAppender appender = config.getAppender("services");
        OutputStreamManager manager = appender.getManager();
        String name = appender.getClass().getName();
        System.out.println(name);

        try {
            Class<? extends AbstractOutputStreamAppender> appenderClass = appender.getClass();
            Method method = appenderClass.getMethod("newBuilder", null);
            AbstractAppender.Builder builder = (AbstractAppender.Builder) method.invoke(null, null);
            Field[] declaredFields = builder.getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                System.out.println(field.getName());
            }
//            String builderName=name+".Builder";
//            Class<?> clazz = Class.forName(builderName);
//            Constructor constructor =clazz.getConstructor();
////            Constructor<AbstractOutputStreamAppender.Builder> constructor = appender.getClass().getConstructor();
//            AbstractOutputStreamAppender.Builder builder = (AbstractOutputStreamAppender.Builder) constructor.newInstance();
//

        } catch (Exception e) {
            e.printStackTrace();
        }

//        LoggerConfig.createLogger(loggerConfig.isAdditive(),loggerConfig.getLevel(),filename+"subfix",loggerConfig.isIncludeLocation()+"",)

//        Appender appender = config.getAppender("File");

        return null;
    }

}
