package com.henry.plaything.gold.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static final String defaultPattern="yyMMdd";
    private static final DateFormat defaultFormat = new SimpleDateFormat(defaultPattern);

    public static Date parseDate(String dateStr) {
        try {
            return defaultFormat.parse(dateStr);
        } catch (Exception e) {
            return null;
        }
    }

    public static String formateDate(Date date) {
        try {
            return defaultFormat.format(date);
        } catch (Exception e) {
            return null;
        }
    }
}
