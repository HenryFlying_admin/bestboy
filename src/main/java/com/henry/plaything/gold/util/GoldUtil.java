package com.henry.plaything.gold.util;

import com.henry.plaything.gold.model.EndPoint;
import com.henry.plaything.gold.model.LineSegment;

public class GoldUtil {
	/**
	 * 计算反弹的点位
	 * @param line 线段
	 */
	public static void rebound(LineSegment line) {
		System.out.println("*******************************");
		double d1=line.getStart().getPrice();
		double d2=line.getStop().getPrice();
		double change = Math.abs(d1 - d2);
		if (d1 < d2) {
			System.out.println("第零反弹点:" + (d2 - 0.236 * change));
			System.out.println("第一反弹点:" + (d2 - 0.382 * change));
			System.out.println("第二反弹点:" + (d2 - 0.5 * change));
			System.out.println("第三反弹点:" + (d2 - 0.618 * change));
		} else {
			System.out.println("第零反弹点:" + (d2 + 0.236 * change));
			System.out.println("第一反弹点:" + (d2 + 0.382 * change));
			System.out.println("第二反弹点:" + (d2 + 0.5 * change));
			System.out.println("第三反弹点:" + (d2 + 0.618 * change));
		}

	}

	/**
	 * 通过对比2段同向的趋势，预测end2
	 *
	 */
	public static void compare(LineSegment line, EndPoint point) {
		System.out.println("--------------------------------");
		double d1=line.getStart().getPrice();
		double d2=line.getStop().getPrice();
		double change = Math.abs(d1 - d2);
		double d3=point.getPrice();
		if (d1 < d2) {
            System.out.println("收缩等长点:" + (d3 + change*0.618));
			System.out.println("比较等长点:" + (d3 + change));
			System.out.println("扩张等长点:" + (d3 + change / 0.618));
		} else {
            System.out.println("收缩等长点:" + (d3 - change*0.618));
			System.out.println("比较等长点:" + (d3 - change));
			System.out.println("扩张等长点:" + (d3 - change / 0.618));
		}

	}

	/**
	 * 扩张三角形(第一段必须与趋势反向)，预测短期的反转点
	 */
	public static void expand(LineSegment line) {
		System.out.println("################################");
		double d1=line.getStart().getPrice();
		double d2=line.getStop().getPrice();
		double change = Math.abs(d1 - d2);
		if (d1 < d2) {
			System.out.println("扩张反转点:" + (d2-change/0.618));
		} else {
			System.out.println("扩张反转点:" + (d2+change / 0.618));
		}

	}
}
