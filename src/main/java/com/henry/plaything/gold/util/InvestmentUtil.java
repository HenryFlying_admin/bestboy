package com.henry.plaything.gold.util;

public class InvestmentUtil {
	/**
	 * 计算反弹的点位，预测begin2
	 * @param d1 线段起点
	 * @param d2 线段终点
	 */
	public static void rebound(double d1, double d2) {
		System.out.println("*******************************");
		double change = Math.abs(d1 - d2);
		if (d1 < d2) {
			System.out.println("第零反弹点:" + (d2 - 0.236 * change));
			System.out.println("第一反弹点:" + (d2 - 0.382 * change));
			System.out.println("第二反弹点:" + (d2 - 0.5 * change));
			System.out.println("第三反弹点:" + (d2 - 0.618 * change));
		} else {
			System.out.println("第零反弹点:" + (d2 + 0.236 * change));
			System.out.println("第一反弹点:" + (d2 + 0.382 * change));
			System.out.println("第二反弹点:" + (d2 + 0.5 * change));
			System.out.println("第三反弹点:" + (d2 + 0.618 * change));
		}

	}

	/**
	 * 通过对比2段同向的趋势，预测end2
	 * @param d1 线段起点
	 * @param d2 线段终点
	 * @param d3 同向另一个线段的起点
	 */
	public static void compare(double d1, double d2, double d3) {
		System.out.println("--------------------------------");
		double change = Math.abs(d1 - d2);
		if (d1 < d2) {
			System.out.println("比较等长点:" + (d3 + change));
			System.out.println("扩张等长点:" + (d3 + change / 0.618));
		} else {
			System.out.println("比较等长点:" + (d3 - change));
			System.out.println("扩张等长点:" + (d3 - change / 0.618));
		}

	}

	/**
	 * 扩张三角形(第一段必须与趋势反向)，预测短期的反转点
	 * @param d1 线段起点
	 * @param d2 线段终点
	 */
	public static void expand(double d1, double d2) {
		System.out.println("################################");
		double change = Math.abs(d1 - d2);
		if (d1 < d2) {
			System.out.println("扩张反转点:" + (d2-change/0.618));
		} else {
			System.out.println("扩张反转点:" + (d2+change / 0.618));
		}

	}
}
