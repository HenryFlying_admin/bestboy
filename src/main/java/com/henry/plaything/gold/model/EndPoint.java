package com.henry.plaything.gold.model;

import com.henry.plaything.gold.util.DateUtil;

import java.util.Date;

public class EndPoint {
    private Date date;
    private double price;

    public EndPoint(String dateStr, double price) {
        this.date = DateUtil.parseDate(dateStr);
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return '{' +DateUtil.formateDate(date) +
                "," + price +
                '}';
    }
}
