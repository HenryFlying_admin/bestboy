package com.henry.plaything.gold.model;

public class LineSegment {
    private EndPoint start;
    private EndPoint stop;
    private double length;

    public LineSegment(EndPoint start, EndPoint stop) {
        this.start = start;
        this.stop = stop;
        this.length=Math.abs(start.getPrice()-stop.getPrice());
    }

    public EndPoint getStart() {
        return start;
    }

    public EndPoint getStop() {
        return stop;
    }

    public double getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "LineSegment{" +
                "start=" + start +
                ", stop=" + stop +
                ", length=" + length +
                '}';
    }
}
