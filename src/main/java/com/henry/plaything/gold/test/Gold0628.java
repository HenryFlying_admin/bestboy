package com.henry.plaything.gold.test;

import com.henry.plaything.gold.util.InvestmentUtil;

public class Gold0628 {
	public static void main(String[] args) {
		double d1 = 1355.74;
		double d2 = 1281.76;
		InvestmentUtil.rebound(d1, d2);

		double d3 = 1309.30;
		InvestmentUtil.compare(d1, d2, d3);
	}
}
