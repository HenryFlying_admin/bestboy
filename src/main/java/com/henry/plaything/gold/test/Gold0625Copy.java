package com.henry.plaything.gold.test;

import com.henry.plaything.gold.util.InvestmentUtil;

public class Gold0625Copy {
	public static void main(String[] args) {
		double d1 = 1309.30;
		double d2 = 1260.84;
		InvestmentUtil.rebound(d1, d2);

		double d3 = 1272.54;
		InvestmentUtil.compare(d1, d2, d3);
	}
}
