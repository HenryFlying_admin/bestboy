package com.henry.plaything.gold.test;

import com.henry.plaything.gold.util.InvestmentUtil;

public class Gold0718 {
	public static void main(String[] args) {
		double d1 = 1237.32;
		double d2 = 1265.87;
		InvestmentUtil.expand(d1, d2);

		double d3 = 1265.87;
		double d4 = 1220.86;
		InvestmentUtil.expand(d3, d4);
	}
}
