package com.henry.plaything.gold.main;

import com.henry.plaything.gold.model.EndPoint;
import com.henry.plaything.gold.model.LineSegment;
import com.henry.plaything.gold.util.GoldUtil;
import com.henry.plaything.gold.util.InvestmentUtil;
import org.junit.Test;

public class TestMain {
    public static void main(String[] args){
        InvestmentUtil.rebound(1365.28,1333.84);
        InvestmentUtil.compare(1365.28,1333.84,1354.76);
        InvestmentUtil.rebound(1354.76,1322.18);
        InvestmentUtil.compare(1354.76,1322.18,1332.74);
//        InvestmentUtil.compare(1364.82,1333.50,1355.50);

        InvestmentUtil.rebound(1365.28,1301.84);
        InvestmentUtil.compare(1365.28,1301.84,1326.04);
        InvestmentUtil.rebound(1326.04,1282.03);
        InvestmentUtil.compare(1326.04,1282.03,1309.40);


        InvestmentUtil.rebound(1365.28,1238.02);
        InvestmentUtil.compare(1365.28,1238.02,1266.02);

        InvestmentUtil.rebound(1354.76,1238.02);
        InvestmentUtil.compare(1354.76,1238.02,1266.02);
    }
    @Test
    public void fixFun(){
        EndPoint g_i = new EndPoint("180411", 1365.23);
        InvestmentUtil.rebound(1365.23,1333.56);
        InvestmentUtil.compare(1365.23,1333.56,1354.70);

        InvestmentUtil.rebound(1354.70,1301.51);
        InvestmentUtil.compare(1354.70,1301.51,1325.96);
//        InvestmentUtil.compare(1365.23,1333.56,1355.74);
    }

    @Test
    public void fixFun2(){
        LineSegment s_i=new LineSegment(new EndPoint("180411", 1365.23),new EndPoint("180412",1333.56));
//        LineSegment s_i=new LineSegment(new EndPoint("180419", 1355.74),new EndPoint("180419",1355.74));
//        LineSegment s_ii=new LineSegment(s_i.getStop(),new EndPoint("180419", 1354.70));
        LineSegment s_ii=new LineSegment(s_i.getStop(),new EndPoint("180419", 1355.74));
        LineSegment s_iii=new LineSegment(s_ii.getStop(),new EndPoint("180501", 1301.51));
        LineSegment s_iv=new LineSegment(s_iii.getStop(),new EndPoint("180511", 1325.96));
        LineSegment s_v=new LineSegment(s_iv.getStop(),new EndPoint("180521", 1281.76));


        LineSegment s_1 = new LineSegment(s_i.getStart(),s_v.getStop());
        LineSegment s_2 = new LineSegment(s_1.getStop(),new EndPoint("180613",1309.30));
        LineSegment s_3 = new LineSegment(s_2.getStop(),new EndPoint("180816",1159.96));
        LineSegment s_4 = new LineSegment(s_3.getStop(),new EndPoint("181015",1233.26));

//        GoldUtil.compare(s_i,s_iv.getStop());
//        GoldUtil.compare(s_iii,s_iv.getStop());
//        GoldUtil.rebound(s_1);
        GoldUtil.rebound(s_3);
//        GoldUtil.compare(s_1,s_2.getStop());
//        GoldUtil.compare(s_3,s_4.getStop());
        GoldUtil.compare(s_1,s_4.getStop());

        System.out.println(s_1);
        System.out.println(s_2);
        System.out.println(s_3);
        System.out.println(s_4);
//        System.out.println(s_4.getLength());
//        InvestmentUtil.compare(1365.23,1333.56,1355.74);

//        System.out.println(s_1.getStop().getPrice()-1.618*s_1.getLength());
//        System.out.println(s_3.getStop().getPrice()+0.618*(s_3.getStop().getPrice()-s_1.getStart().getPrice()));
//        System.out.println(s_3.getStop().getPrice()+0.382*(s_3.getStop().getPrice()-s_1.getStart().getPrice()));
    }
}
