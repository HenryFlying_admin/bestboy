package com.leetcode.algorithms.one.problem6;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 *
 * Input: s = "PAYPALISHIRING", numRows = 3
 * Output: "PAHNAPLSIIGYIR"
 */
public class ZigZagConversion {
    @Test
    public void run(){
        String s = "AB";
        int numRows =1;

        System.out.println(convert(s,numRows));
    }

    public String convert(String s, int numRows) {
        if(numRows<=1)
            return s;
        List<StringBuilder > lines = new ArrayList<>();
        for(int i=0;i<numRows;i++)
            lines.add(new StringBuilder());
        int k=0;
        boolean flag = true;
        for(int i=0;i<s.length();i++){
            if(k==numRows-1)
                flag=false;
            if(k==0)
                flag=true;

            if(flag){
               lines.get(k++).append(s.charAt(i));
            }else{
                lines.get(k--).append(s.charAt(i));
            }
        }
        StringBuilder sb = new StringBuilder();
        for(StringBuilder line :lines){
            sb.append(line.toString());
        }


        return sb.toString();
    }
}
