package com.leetcode.algorithms.one.problem1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
/**
 * 给一个int[]数组，一个目标数，从int[]数组中取2个数相加使和等于目标数，返回这2个下标组成的int数组
 * @author Henry
 *
 */
public class TwoSum {
	
	public static int[] twoSum(int[] nums,int target){
		Map<Integer,Integer> map = new HashMap<>();
		map.put(nums[0], 0);
		int val;
		for(int i=1;i<nums.length;i++){
			val =target-nums[i];
			if(map.containsKey(val)){
				return new int[]{map.get(val),i};
			}
			map.put(nums[i], i);
		}
		throw new IllegalArgumentException("No two sum solution");
	}
	
	public static void main(String[] args) {
		int[] nums ={1,2,4};
		int target=6;
		System.out.println(Arrays.toString(twoSum(nums, target)));
		
	}
}
