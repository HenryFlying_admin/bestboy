package com.leetcode.algorithms.one.problem9;

public class PalindromeNumber {
    public boolean isPalindrome(int x) {
        if(x<0)
            return false;
        if(x>=0&&x<=9)
            return true;
        String s = x+"";
        int len0=s.length();
        int len = len0 / 2;
        for(int i=0;i<len;i++){
            if (!(s.charAt(i) == s.charAt(len0 - i - 1))) {
                return false;
            }
        }
        return true;
    }
}
