package com.leetcode.algorithms.one.problem2;

public class ListNode {
	int val;
    ListNode next;
    ListNode(int x) { val = x; }
    
    @Override
	public String toString(){
		StringBuilder sb = new StringBuilder(val+"");
		ListNode next = this.next;
		while(next!=null){
			sb.insert(0, next.val);
			next = next.next;
		}
		return sb.toString();
	}
}
