package com.leetcode.algorithms.two.problem11;

import org.junit.Test;

public class ContainerWithMostWater {
    @Test
    public void run(){
        int[] arr = {1,2,3,4,5,25,24,3,4};
        System.out.println(maxArea1(arr));
    }


    public int maxArea(int[] height) {
        int max = 0;
        for(int i=0;i<height.length-1;i++){
            for(int j=i+1;j<height.length;j++){
                int h = height[i]>height[j]?height[j]:height[i];
                int curr = (j - i) *h;
                max=max>curr?max:curr;
            }
        }

        return max;
    }

    public int maxArea1(int[] height) {
        int max = 0;
        int h1=0;
        for(int i=0;i<height.length-1;i++){
            if(height[i]<h1){
                continue;
            }else {
                h1=height[i];
            }
            int h2=0;
            for(int j=height.length-1;j>i;j--){
                if(height[j]<h2){
                    continue;
                }else {
                    h2=height[j];
                }
                int h = height[i]>height[j]?height[j]:height[i];
                int curr = (j - i) *h;
                max=max>curr?max:curr;
            }
        }

        return max;
    }

    /**
     * 答案很巧妙  拨动柱子算法
     * @param height
     * @return
     */
    public int maxArea2(int[] height) {
        int maxarea = 0, l = 0, r = height.length - 1;
        while (l < r) {
            maxarea = Math.max(maxarea, Math.min(height[l], height[r]) * (r - l));
            if (height[l] < height[r])
                l++;
            else
                r--;
        }
        return maxarea;
    }

}
