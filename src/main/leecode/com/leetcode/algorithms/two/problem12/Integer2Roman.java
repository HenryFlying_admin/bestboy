package com.leetcode.algorithms.two.problem12;

import org.junit.Test;

public class Integer2Roman {
    @Test
    public void run(){
        int num=1994;
        System.out.println(intToRoman(num));
    }

    public String intToRoman1(int num) {
        int values[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        String str[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < values.length; i++) {
            while(num >= values[i]) {
                num -= values[i];
                sb.append(str[i]);
            }
        }

        return sb.toString();
    }


    public String intToRoman(int num) {
        StringBuilder sb=new StringBuilder();
        if(num>=1000){
            int k = num/1000;
            for(int i=0;i<k;i++)
                sb.append("M");
            num=num-1000*k;
        }

        if(num>=900){
            sb.append("CM");
            num=num-900;
        }

        if(num>=500){
            sb.append("D");
            num=num-500;
        }
        if(num>=400){
            sb.append("CD");
            num=num-400;
        }
        if(num>=100){
            int k=num/100;
            for(int i=0;i<k;i++)
                sb.append("C");
            num=num-100*k;
        }
        if(num>=90){
            sb.append("XC");
            num=num-90;
        }

        if(num>=50){
            sb.append("L");
            num=num-50;
        }
        if(num>=40){
            sb.append("XL");
            num=num-40;
        }
        if(num>=10){
            int k=num/10;
            for(int i=0;i<k;i++)
                sb.append("X");
            num=num-10*k;
        }

        if(num>=9){
            sb.append("IX");
            num=num-9;
        }

        if(num>=5){
            sb.append("V");
            num=num-5;
        }
        if(num>=4){
            sb.append("IV");
            num=num-4;
        }
        if(num>=1){
            int k=num/1;
            for(int i=0;i<k;i++)
                sb.append("I");
            num=num-10*k;
        }


        return sb.toString();
    }
}
