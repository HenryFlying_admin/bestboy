package com.leetcode.algorithms.two.problem15;

import org.junit.Test;

import java.util.*;

/**
 * a+b+c=0
 * 找出所有的结果,还要去重
 */
public class ThreeSum {

    @Test
    public void run(){
        int[] nums = {3,0,-2,-1,1,2};
        System.out.println(threeSum(nums));
    }

    public List<List<Integer>> threeSum(int[] nums) {
        if(nums==null||nums.length<3)
            return null;
        Set<List<Integer>> set = new HashSet<>();

        for(int i=0;i<nums.length-2;i++){

            for(int j=i+1;j<nums.length-1;j++){

                int want = 0-(nums[i]+nums[j]);
                label:
                for(int k=j+1;k<nums.length;k++){
                    if (want == nums[k]) {
                        ArrayList<Integer> list = new ArrayList<>();
                        list.add(nums[i]);
                        list.add(nums[j]);
                        list.add(nums[k]);
                        Collections.sort(list);
                        set.add(list);
                        break label;
                    }
                }
            }
        }
        return new ArrayList<List<Integer>>(set);

    }
}
