package com.leetcode.recursion;

/**
 * �ҳ�쳲��������е�indexλ��ֵ
 * @author Henry
 *
 */
public class Fibonacci {
	
	//1,1,2,3,5,8,13,21,34,55
	public static int getFibonacci(int index){
		if(index==0||index==1){
			return 1;
		}
		return getFibonacci(index-1)+getFibonacci(index-2);
		
	}
	
	public static void main(String[] args) {
		for(int i=0;i<10;i++){
			System.out.println(getFibonacci(i));
		}
	}
}
