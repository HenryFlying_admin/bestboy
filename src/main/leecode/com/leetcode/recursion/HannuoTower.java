package com.leetcode.recursion;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class HannuoTower {
//    private static final Logger logger = LogManager.getLogger("console");
    private static final Logger logger = LogManager.getLogger("services");
    private static final String DISK_A = "A";
    private static final String DISK_B ="B";
    private static final String DISK_C="C";

    static String from = DISK_A;
    static String to = DISK_C;
    static String mid = DISK_B;

    public static void main(String[] args){
        System.out.println("游戏开始，请输入汉诺塔的层数！");
        String input = JOptionPane.showInputDialog("please input the number of the disks you want me move.");
        int num = Integer.parseInt(input);
        System.out.println("您输入的汉诺塔层数为"+num);
		move(num, from, mid, to);
        System.out.println("游戏结束---------------");

    }

    private static void move(int num,String from2,String mid2,String to2){
        if(num ==1){
            //如果只有一个盘子  直接从A移到C  结束
            logger.info("move disk 1 from {} to {}",from2,to2);
            System.out.println();
        }else{

            //先把n-1个盘子   从A移到B
            move(num-1, from2, to2, mid2);
            //move函数会先把n-1个盘子从A移到B后,会把最下面的盘子从A移到C
            logger.info("move disk {} from {} to {}",num,from2,to2);
            System.out.println();
            //再把n-1个盘子从B移到C
            move(num-1, mid2, from2, to2);
        }
    }
}
